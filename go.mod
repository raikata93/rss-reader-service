module rss-reader-service

go 1.19

require (
	github.com/NeowayLabs/wabbit v0.0.0-20210927194032-73ad61d1620e
	github.com/raikata93/ssh-reader v1.0.9
	github.com/streadway/amqp v1.0.0
)

require (
	github.com/araddon/dateparse v0.0.0-20210429162001-6b43995a97de // indirect
	github.com/fsouza/go-dockerclient v1.9.4 // indirect
	github.com/google/uuid v1.0.0 // indirect
	github.com/pborman/uuid v1.2.1 // indirect
	github.com/rabbitmq/amqp091-go v1.7.0 // indirect
	github.com/tiago4orion/conjure v0.0.0-20150908101743-93cb30b9d218 // indirect
)
