package handler

import (
	"github.com/NeowayLabs/wabbit/amqptest"
	"github.com/NeowayLabs/wabbit/amqptest/server"
	"testing"
)

func TestChannelCreation(t *testing.T) {
	mockConn, err := amqptest.Dial("amqp://localhost:5672/%2f")

	if err == nil {
		t.Error("This shall fail, because no fake amqp server is running...")
	}

	fakeServer := server.NewServer("amqp://localhost:5672/%2f")
	fakeServer.Start()

	mockConn, err = amqptest.Dial("amqp://localhost:5672/%2f")

	if err != nil {
		t.Error(err)
	}

	if err != nil {
		t.Error(err)
		return
	}

	if mockConn == nil {
		t.Error("Invalid mockConn")
		return
	}
}
