package handler

import (
	"encoding/json"
	"fmt"
	"github.com/raikata93/ssh-reader/reader"
	"github.com/streadway/amqp"
	"time"
)

type response struct {
	Items []items `json:"items"`
}

type items struct {
	Title       string    `json:"title"`
	Source      string    `json:"source"`
	SourceURL   string    `json:"source_url"`
	Link        string    `json:"link"`
	PublishDate time.Time `json:"publish_date"`
	Description string    `json:"description"`
}

func Consumer() error {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		return err

	}
	defer func(conn *amqp.Connection) {
		err := conn.Close()
		if err != nil {
			return
		}
	}(conn)

	ch, err := conn.Channel()
	if err != nil {
		return err
	}
	defer func(ch *amqp.Channel) {
		err := ch.Close()
		if err != nil {
			return
		}
	}(ch)

	msgs, err := ch.Consume("rss-urls", "", true, false, false, false, nil)
	forever := make(chan bool)
	go func() {
		for d := range msgs {
			fmt.Println(string(d.Body))
			err := parseDataAndProduce(d.Body)
			if err != nil {
				return
			}
		}
	}()

	fmt.Println("[*] - waiting for messages")
	<-forever
	fmt.Println("Successfully consume message")

	return nil
}

func parseDataAndProduce(body []byte) error {
	produceData, err := reader.Parse(string(body[:]))
	if err != nil {
		return fmt.Errorf("error during reader.Parse: %s", err)
	}
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		return fmt.Errorf("error during amqp.Dia: %s", err)
	}
	defer func(conn *amqp.Connection) {
		err := conn.Close()
		if err != nil {
			return
		}
	}(conn)

	ch, err := conn.Channel()
	if err != nil {
		return fmt.Errorf("error during conn.Channel: %s", err)
	}
	defer func(ch *amqp.Channel) {
		err := ch.Close()
		if err != nil {
			return
		}
	}(ch)

	_, err = ch.QueueDeclare("rss-data", false, false, false, false, nil)
	if err != nil {
		return fmt.Errorf("error during ch.QueueDeclare: %s", err)
	}

	var responseItems []items
	for _, item := range produceData {
		responseItems = append(responseItems, items(item))
	}

	modifiedBytes, err := json.Marshal(response{Items: responseItems})
	if err != nil {
		return fmt.Errorf("error during json.Marshal: %s", err)

	}
	err = ch.Publish("", "rss-data", false, false, amqp.Publishing{
		ContentType: "application/json",
		Body:        modifiedBytes,
	})
	if err != nil {
		return fmt.Errorf("error during ch.Publish: %s", err)
	}

	fmt.Println("Successfully publish message to queue")
	return nil
}
