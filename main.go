package main

import (
	"fmt"
	"rss-reader-service/handler"
)

func main() {
	fmt.Println("RSS reader service")
	err := handler.Consumer()
	if err != nil {
		return
	}
}
