FROM golang:1.19

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY *.go ./
COPY handler ./handler

RUN go build -o /go-rss-reader

EXPOSE 8080

CMD [ "/go-rss-reader" ]